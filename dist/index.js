"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Agent_1 = require("./Agent");
// Check command line arguments:
if (process.argv.length < 3) {
    console.error('Arguments should be <name> <port> [<path>]');
}
else {
    // Create this agent and start announcing:
    new Agent_1.Agent({
        name: process.argv[2],
        port: parseInt(process.argv[3]),
        path: process.argv[4] || '/'
    })
        .startAnnouncing();
}
