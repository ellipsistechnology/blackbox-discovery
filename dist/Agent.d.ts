export interface AgentLike {
    name: string;
    path: string;
    port: number;
    address?: string;
    protocol?: string;
}
export declare class Agent implements AgentLike {
    name: string;
    path: string;
    port: number;
    address?: string;
    protocol?: string;
    peers: {
        [key: string]: AgentLike;
    };
    listeners: ((agent: AgentLike) => void)[];
    private announceHandle;
    private socket;
    constructor({ name, path, port, address, protocol }: AgentLike);
    private receive;
    private announceMessage;
    announce(): void;
    startAnnouncing(): void;
    stopAnnouncing(): void;
}
