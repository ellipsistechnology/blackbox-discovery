"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Agent = void 0;
var dgram_1 = require("dgram");
var BROADCAST_ADDRESS = '255.255.255.255';
var BROADCAST_PORT = 5555;
var ANNOUNCE_PREFIX = "BB ANNOUNCE";
var BROADCAST_INTERVAL = 60000;
var Agent = /** @class */ (function () {
    function Agent(_a) {
        var name = _a.name, path = _a.path, port = _a.port, address = _a.address, protocol = _a.protocol;
        this.listeners = [];
        this.name = name;
        this.port = port;
        this.path = path;
        this.address = address;
        this.protocol = protocol;
        this.peers = {};
        this.socket = dgram_1.createSocket('udp4');
        this.socket.on("message", this.receive.bind(this));
    }
    Agent.prototype.receive = function (data, rinfo) {
        var message = data.toString();
        if (message.startsWith(ANNOUNCE_PREFIX)) {
            var headerAndBody = message.split('\n');
            var json_1 = JSON.parse(headerAndBody[1]);
            if (json_1.name && json_1.path && json_1.port) {
                json_1.address = rinfo.address;
                this.peers[json_1.name] = json_1;
                this.listeners.forEach(function (listener) { return listener(json_1); });
                console.log("Discovered peer '" + json_1.name + "' at " + (this.peers[json_1.name].protocol || 'http') + "://" + this.peers[json_1.name].address + ":" + this.peers[json_1.name].port + this.peers[json_1.name].path);
            }
        }
    };
    Agent.prototype.announceMessage = function () {
        return ANNOUNCE_PREFIX + "\n" + JSON.stringify({
            name: this.name,
            path: this.path,
            port: this.port,
            address: this.address
        });
    };
    Agent.prototype.announce = function () {
        var announceMessage = this.announceMessage();
        this.socket.send(Buffer.from(announceMessage), 0, announceMessage.length, BROADCAST_PORT, BROADCAST_ADDRESS, function (err) {
            if (err)
                console.log(err);
            else
                console.log("Message sent");
        });
    };
    Agent.prototype.startAnnouncing = function () {
        var _this = this;
        if (this.announceHandle) {
            return;
        }
        this.socket.bind(BROADCAST_PORT, '0.0.0.0', function () {
            _this.socket.setBroadcast(true);
            _this.announce();
            _this.announceHandle = setInterval(_this.announce.bind(_this), BROADCAST_INTERVAL);
        });
    };
    Agent.prototype.stopAnnouncing = function () {
        clearInterval(this.announceHandle);
        this.announceHandle = 0;
    };
    return Agent;
}());
exports.Agent = Agent;
