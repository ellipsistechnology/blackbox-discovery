import { Agent } from "./Agent"

// Check command line arguments:
if(process.argv.length < 3) {
  console.error('Arguments should be <name> <port> [<path>]')
}
else {
  // Create this agent and start announcing:
  new Agent({
    name: process.argv[2],
    port: parseInt(process.argv[3]),
    path: process.argv[4] || '/'
  })
  .startAnnouncing()
}
