import {createSocket, RemoteInfo, Socket} from 'dgram'

const BROADCAST_ADDRESS = '255.255.255.255';
const BROADCAST_PORT = 5555;
const ANNOUNCE_PREFIX = "BB ANNOUNCE"
const BROADCAST_INTERVAL = 60000

export interface AgentLike {
  name: string
  path: string
  port: number
  address?: string
  protocol?: string
}

export class Agent implements AgentLike {
  name: string
  path: string
  port: number
  address?: string
  protocol?: string

  peers: {[key: string]: AgentLike}
  listeners: ((agent: AgentLike)=>void)[] = []
  
  private announceHandle: any
  private socket: Socket

  constructor({name, path, port, address, protocol}: AgentLike) {
    this.name = name
    this.port = port
    this.path = path
    this.address = address
    this.protocol = protocol

    this.peers = {}

    this.socket = createSocket('udp4');
    this.socket.on("message", this.receive.bind(this))
  }

  private receive( data: Buffer, rinfo: RemoteInfo ) {
    const message = data.toString()
    if(message.startsWith(ANNOUNCE_PREFIX)) {
      const headerAndBody = message.split('\n')
      const json = JSON.parse(headerAndBody[1])
      if(json.name && json.path && json.port) {
        json.address = rinfo.address
        this.peers[json.name] = json
        this.listeners.forEach(listener => listener(json))
        console.log(`Discovered peer '${json.name}' at ${this.peers[json.name].protocol||'http'}://${this.peers[json.name].address}:${this.peers[json.name].port}${this.peers[json.name].path}`)
      }
    }
  }

  private announceMessage() {
    return ANNOUNCE_PREFIX + "\n" + JSON.stringify({
      name: this.name,
      path: this.path,
      port: this.port,
      address: this.address
    })
  }

  announce() {
    const announceMessage = this.announceMessage()
    this.socket.send(Buffer.from(announceMessage), 
        0, 
        announceMessage.length, 
        BROADCAST_PORT, 
        BROADCAST_ADDRESS, 
        function (err) {
          if (err) 
            console.log(err);
          else
            console.log("Message sent");
        }
    );
  }
  
  startAnnouncing() {
    if(this.announceHandle) {
      return
    }

    this.socket.bind(BROADCAST_PORT, '0.0.0.0', () => {
      this.socket.setBroadcast(true)

      this.announce()
      this.announceHandle = setInterval(this.announce.bind(this), BROADCAST_INTERVAL)
    })
  }

  stopAnnouncing() {
    clearInterval(this.announceHandle)
    this.announceHandle = 0
  }
}